/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Sample/test code for running a user program.  You can use this for
 * reference when implementing the execv() system call. Remember though
 * that execv() needs to do more than this function does.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vm.h>
#include <vfs.h>
#include <syscall.h>
#include <test.h>
#include "opt-A2.h"
#if OPT_A2
#include <copyinout.h>
#include <mips/trapframe.h>
#include <kern/fcntl.h>
#include <vm.h>
#include <vfs.h>
#include <test.h>
#endif

/*
 * Load program "progname" and start running it in usermode.
 * Does not return except on error.
 *
 * Calls vfs_open on progname and thus may destroy it.
 */

// so runprogram's caller is already passing it user arguments, we just need to hangle it now
// yeah im using a past explanation of this assignment + leslie's videos

// get args from address space of original program (before we destroy it), and set up an ARGUMENT ARRAY in address space of new program
// i assume this is what the copyinout.c is for?
// we need to decide where in address space to place this. I'd assume at the top after code? somewhere where it won't get overwritten

// oh we do execv first because arguments here come from kernel got it

#if !OPT_A2
int runprogram(char *progname)
#endif
#if OPT_A2
int runprogram(char *progname, char **args, unsigned long nargs)
#endif
{
	(void)args;
	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;

	#if OPT_A2
		// Use copyin/copyout for fixed size variables (integers, arrays, etc.). This is a pointer to a pointer. 

		// count number of args
		int argc = nargs;

		if(argc == 9) {
			argc = 8;
		}

		// end
	#endif

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	KASSERT(curproc_getas() == NULL);

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	curproc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

	#if OPT_A2
	// BEGIN: Copy to userspace (stack)

	// notice it follows similar structure in how we copy as kernel

	vaddr_t stackptr_copy = stackptr;
	if(args != NULL && argc != 0) {
		// This vector is a NULL terminated array of addresses,
		vaddr_t *stack_args = kmalloc((argc + 1) * sizeof(*stack_args));

		for (int i = argc-1; i >= 0; i--) {
			// place the arguments after the stack pointer, decrementing the pointer to make room for args
			// Since userspace will never increment the stack pointer beyond the initial value we pass to it
			stackptr_copy -= ROUNDUP(strlen(args[i]) + 1, 4);
			// Expect a 4-byte boundary: so round down the stack pointer to a multiple of 4 before copying it out

			// Copy in each arg string (remember need to allocate before copyinstr). Notice it's being copied to stack, not kmalloc and not the stack_args array
			result = copyoutstr(args[i], (userptr_t)stackptr_copy, strlen(args[i]) + 1, NULL);
			if(result) {
				return result;
			}


			// This vector is a NULL terminated array of addresses, thats what its actually storing
			stack_args[i] = stackptr_copy;
		}

		// NULL terminated
		stack_args[argc] = (vaddr_t) NULL;

		for (int i = argc; i >= 0; --i) {
			// place the arguments after the stack pointer, decrementing the pointer to make room for args
			stackptr_copy -= ROUNDUP(sizeof(*stack_args), 4);

			// Then copy in each of the char pointer values (pointer to an arg) (won't get a NULL because we stop argc before that)
			result = copyout((void*)&stack_args[i], (userptr_t)stackptr_copy, sizeof *stack_args);
			if(result) {
				return result;
			}
		}

		kfree(stack_args);
	}

	// END: Copy to userspace (stack)
	#endif

	if(argc == 1 || args == NULL) {
		enter_new_process(0 /*argc*/, NULL /*userspace addr of argv*/,
				stackptr, entrypoint);
	}
	enter_new_process(argc /*argc*/, (userptr_t) stackptr_copy /*userspace addr of argv*/, stackptr_copy, entrypoint);

	
	
	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
	// struct addrspace *as;
	// struct vnode *v;
	// vaddr_t entrypoint, stackptr;
	// int result;

	// #if OPT_A2
	// // Use copyin/copyout for fixed size variables (integers, arrays, etc.). This is a pointer to a pointer. 

	// // count number of args
	// int argc = 0;
	// int j = 0;
	// while(args[j] != NULL) {
	// 	argc++;
	// 	j++;
	// }

	// if(argc == 9) {
	// 	argc = 8;
	// }

	// // end
	// #endif

	// /* Open the file. */
	// char *fname_temp;
	// fname_temp = kstrdup(progname);
	// 	result = vfs_open(fname_temp, O_RDONLY, 0, &v);
	// 	if (result) {
	// 		return result;
	// 	}
	// kfree(fname_temp);
	// /* We should be a new process. */
	// KASSERT(curproc_getas() == NULL);

	// /* Create a new address space. */
	// as = as_create();
	// if (as ==NULL) {
	// 	vfs_close(v);
	// 	return ENOMEM;
	// }

	// /* Switch to it and activate it. */
	// curproc_setas(as);
	// as_activate();

	// /* Load the executable. */
	// result = load_elf(v, &entrypoint);
	// if (result) {
	// 	/* p_addrspace will go away when curproc is destroyed */
	// 	vfs_close(v);
	// 	return result;
	// }

	// /* Done with the file now. */
	// vfs_close(v);

	// /* Define the user stack in the address space */
	// result = as_define_stack(as, &stackptr);
	// if (result) {
	// 	/* p_addrspace will go away when curproc is destroyed */
	// 	return result;
	// }

	// #if OPT_A2
	// // BEGIN: Copy to userspace (stack)

	// // notice it follows similar structure in how we copy as kernel

	// vaddr_t stackptr_copy = stackptr;
	// if(args != NULL && argc != 0) {
	// 	// This vector is a NULL terminated array of addresses,
	// 	vaddr_t *stack_args = kmalloc((argc + 1) * sizeof(*stack_args));

	// 	for (int i = argc-1; i >= 0; i--) {
	// 		// place the arguments after the stack pointer, decrementing the pointer to make room for args
	// 		// Since userspace will never increment the stack pointer beyond the initial value we pass to it
	// 		stackptr_copy -= ROUNDUP(strlen(args[i]) + 1, 4);
	// 		// Expect a 4-byte boundary: so round down the stack pointer to a multiple of 4 before copying it out

	// 		// Copy in each arg string (remember need to allocate before copyinstr). Notice it's being copied to stack, not kmalloc and not the stack_args array
	// 		result = copyoutstr(args[i], (userptr_t)stackptr_copy, strlen(args[i]) + 1, NULL);
	// 		if(result) {
	// 			return result;
	// 		}


	// 		// This vector is a NULL terminated array of addresses, thats what its actually storing
	// 		stack_args[i] = stackptr_copy;
	// 	}

	// 	// NULL terminated
	// 	stack_args[argc] = (vaddr_t) NULL;

	// 	for (int i = argc; i >= 0; --i) {
	// 		// place the arguments after the stack pointer, decrementing the pointer to make room for args
	// 		stackptr_copy -= ROUNDUP(sizeof(*stack_args), 4);

	// 		// Then copy in each of the char pointer values (pointer to an arg) (won't get a NULL because we stop argc before that)
	// 		result = copyout((void*)&stack_args[i], (userptr_t)stackptr_copy, sizeof *stack_args);
	// 		if(result) {
	// 			return result;
	// 		}
	// 	}

	// 	kfree(stack_args);
	// }

	// // END: Copy to userspace (stack)
	// #endif

	// /* Warp to user mode. */
	// enter_new_process(argc /*argc*/, (userptr_t) stackptr_copy /*userspace addr of argv*/, stackptr_copy, entrypoint);
	
	// /* enter_new_process does not return. */
	// panic("enter_new_process returned\n");
	// return EINVAL;
}

