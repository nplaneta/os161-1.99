#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include "opt-A2.h"
#if OPT_A2
#include <mips/trapframe.h>
#include <kern/fcntl.h>
#include <vm.h>
#include <vfs.h>
#include <test.h>
#endif

#if OPT_A2

// need to allocate memory in kernel to store arg info!

int
sys_execv(userptr_t progname, userptr_t argv)
{
  // never trust a user address, do not assume pointer is valid, use special functions to check validity
  // so use copyin and copyout to touch array, and copyinstr for NULL terminated strings
  // allocate space?
  // copy program name into kernel space
  // copy in each of the char pointer values untily ou get a NULL pointer
  // then copy in each arg string

	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;

  // BEGIN: Copy to kernel

  int MAX_LENGTH = 128;

  // first copy program name into kernel space
  char* kernel_progname = kmalloc((strlen((char *)progname) + 1) * sizeof(char));
  KASSERT(kernel_progname != NULL);
  result = copyinstr(progname, kernel_progname, (strlen((char *)progname) + 1) * sizeof(char), NULL);
  KASSERT(result == 0);
  // end

  // Use copyin/copyout for fixed size variables (integers, arrays, etc.). This is a pointer to a pointer. 
  char** args = (char**) argv;  

  // count number of args
	int argc = 0;
  int j = 0;
	while(args[j] != NULL) {
		argc++;
		j++;
	}
  // end

  char **kernel_args;

  if(argv != NULL && argc != 0) {
    // Next copy in each of the char pointer values (pointer to an arg) until you get a NULL pointer. +1 is for NULL at end of string
  kernel_args = kmalloc(sizeof(char *) * (argc + 1));
  // (Don't forget to allocate space for the pointer array and the strings, thats what's above)

  for(int i = 0; i < argc; i++) {
    size_t length = strlen(args[i]) + 1;
    char *temp_to_store_charpointer_values = NULL;

    // copy in each of the char pointer values (pointer to an arg) (won't get a NULL because we stop argc before that)
    copyin((void*)&kernel_args[i], &temp_to_store_charpointer_values, sizeof* kernel_args);

    // Then copy in each arg string (remember need to allocate before copyinstr)
    kernel_args[i] = kmalloc((strlen(args[i]) + 1) * sizeof(*kernel_args));
    result = copyinstr((userptr_t) args[i], kernel_args[i], MAX_LENGTH, &length);
    //      usersrc, must be userptr        second arg is destination, which is in kernel, which is why its pointer to allocated memory

    if(result) {
        return result;
    }
  }

  // NULL terminated array
  kernel_args[argc] = NULL;

  }

  // END: Copy to kernel

	/* Open the file. */
  char *fname_temp;
  fname_temp = kstrdup(kernel_progname);
	result = vfs_open(fname_temp, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}
  kfree(fname_temp);

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	struct addrspace * as_old = curproc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

  // BEGIN: Copy to userspace (stack)

  // notice it follows similar structure in how we copy as kernel

  vaddr_t stackptr_copy = stackptr;
  vaddr_t *stack_args;
  if(argv != NULL && argc != 0) {
    // This vector is a NULL terminated array of addresses,
    stack_args = kmalloc((argc + 1) * sizeof(*stack_args));

    for (int i = argc-1; i >= 0; i--) {
      // place the arguments after the stack pointer, decrementing the pointer to make room for args
      // Since userspace will never increment the stack pointer beyond the initial value we pass to it
      stackptr_copy -= ROUNDUP(strlen(kernel_args[i]) + 1, 8);
      // Expect a 4-byte boundary: so round down the stack pointer to a multiple of 4 before copying it out

      // Copy in each arg string (remember need to allocate before copyinstr). Notice it's being copied to stack, not kmalloc and not the stack_args array
      copyoutstr(kernel_args[i], (userptr_t)stackptr_copy, strlen(kernel_args[i]) + 1, NULL);

      // This vector is a NULL terminated array of addresses, thats what its actually storing
      stack_args[i] = stackptr_copy;
    }

    // NULL terminated
    stack_args[argc] = (vaddr_t) NULL;

    for (int i = argc; i >= 0; --i) {
      // place the arguments after the stack pointer, decrementing the pointer to make room for args
      stackptr_copy -= sizeof *stack_args;

      // Then copy in each of the char pointer values (pointer to an arg) (won't get a NULL because we stop argc before that)
      result = copyout((void*)&stack_args[i], (userptr_t)stackptr_copy, sizeof *stack_args);
    }
  }

  // END: Copy to userspace (stack)


  as_destroy(as_old);
  kfree(kernel_progname);
  if(argv != NULL && argc != 0) {
    for(int i = 0; i < argc; i++) {
          kfree(kernel_args[i]);
    }
    kfree(kernel_args);
    kfree(stack_args);
  }

  // for some reason trying to 8-bit align the stackptr does not rlly work
	/* Warp to user mode. */
  if(argc == 1 || args == NULL) {
		/* Warp to user mode. */
		enter_new_process(0 /*argc*/, NULL /*userspace addr of argv*/,
				stackptr, entrypoint);
	} else {
    enter_new_process(argc /*argc*/, (userptr_t) stackptr_copy /*userspace addr of argv*/, stackptr_copy, entrypoint);
  }

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
  return EINVAL;
}


void
enter_forked_process_wrapper(void *goingtobetrapframe, unsigned long filler)
{
	(void)filler;
	as_activate();
	enter_forked_process(goingtobetrapframe);
}

int sys_fork(struct trapframe *trapframe, pid_t *retval) {

  struct proc *child_proc = NULL;

  child_proc = proc_create_runprogram("child");

   if(child_proc == NULL) {
    return ENOMEM;
  }
  
  child_proc->processID = getfromglobal();

  child_proc->parent = curproc;

  child_proc->childrendoneflag = 1;

  // child_proc->all_children = NULL;  THIS MAKES EVERYTHING BREAK AND I HAVE NO FRICKING CLUE WHY

  struct procdata *child_proc_info = kmalloc(sizeof(struct procdata));
  child_proc_info->proc_addr = child_proc;
  child_proc_info->processID = child_proc->processID;
  child_proc_info->p_exit_code = -1;
  child_proc_info->waitingonme = 0;
  array_add(curproc->all_children, child_proc_info, NULL);


  as_copy(curproc_getas(), &child_proc->p_addrspace); //why wouldn't this just set process addressspace

  if(child_proc->p_addrspace == NULL) {
    return ENOMEM;
  }

  // HOW TO DO THIS Easier approach: copy the parent's trap frame to the kernel’s heap, then copy from kernel’s heap to child
  struct trapframe *tf_copy = kmalloc(sizeof(struct trapframe));
	memcpy((void *)tf_copy, (const void*) trapframe, sizeof(struct trapframe));

  thread_fork(child_proc->p_name, child_proc, enter_forked_process_wrapper, tf_copy, 0); //WHY IS IT UNDEFINED??
  // DOES THIS COUNT AS PASSING TRAPFRAME
  // this build despite error

  *retval =  child_proc->processID; //return value is child, of course parent always return child

  return 0;
  
}


  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);

  int non_exited_processes = 0;

  // reminder to myself because i be dumb, remember how much easier this became when you used debug statements?
  // yeah shouldve done that sooner lmao

  //ok so everything here just makes sure parent can't exit until child does

  // but, I add something in the childcode to deal with waitpid

  lock_acquire(curproc->procdata_lock); 
  if(array_num(curproc->all_children) > 0) { //this means it's a parent with children
    for (int unsigned i = 0; i < array_num(curproc->all_children); i++) {
        struct procdata *temp = array_get(curproc->all_children, i);
        if(temp->p_exit_code == -1) {
          non_exited_processes++;
        }
    }

    if(non_exited_processes > 0) {
      cv_wait(curproc->finish_mutex, curproc->procdata_lock);
    }
  }
  lock_release(curproc->procdata_lock); 

  if(curproc->parent != NULL) { // this is a child so has a parent
    lock_acquire(curproc->parent->procdata_lock);

    for (int unsigned i = 0; i < array_num(curproc->parent->all_children); i++) {
        struct procdata *temp = array_get(curproc->parent->all_children, i);
        if(temp->processID == curproc->processID) {
          temp->p_exit_code = _MKWAIT_EXIT(exitcode);

          if(temp->waitingonme != 0) {
            cv_signal(curproc->parent->waitPID_mutex, curproc->parent->procdata_lock);
          }
          break;
        }
    }

    non_exited_processes = 0;

    for (int unsigned i = 0; i < array_num(curproc->parent->all_children); i++) {
        struct procdata *temp = array_get(curproc->parent->all_children, i);
        if(temp->p_exit_code == -1) {
          non_exited_processes++;
        }
    }

    if(non_exited_processes == 0) {
      cv_signal(curproc->parent->finish_mutex, curproc->parent->procdata_lock);
    }

    lock_release(curproc->parent->procdata_lock);
  }




  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = curproc->processID;
  return 0;
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  if (options != 0) {
    return(EINVAL);
  }

  lock_acquire(curproc->procdata_lock); 
  for (int unsigned i = 0; i < array_num(curproc->all_children); i++) {
        struct procdata *temp = array_get(curproc->all_children, i);
        if(temp->processID == pid) {
          temp->waitingonme = 1; // CRUCIAL RIGHT?
          exitstatus = temp->p_exit_code;
          break;
        }
	}

  if(exitstatus == -1) { // why it's not a while: doesn't actually rely on exit status, but child exit status in struct
  // that struct doesn't change once it's exit status is set diff from original (-1) so I don't need to recheck it
    cv_wait(curproc->waitPID_mutex, curproc->procdata_lock);
  }

  for (int unsigned i = 0; i < array_num(curproc->all_children); i++) {
        struct procdata *temp = array_get(curproc->all_children, i);
        if(temp->processID == pid) {
          temp->waitingonme = 0; 
          exitstatus = temp->p_exit_code;
          break;
        }
	}
  lock_release(curproc->procdata_lock); 

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  /* for now, just pretend the exitstatus is 0 */
  // exitstatus = 0; why didnt u comment this out seriously cmon
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#else

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  *retval = 1;
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#endif // OPT_A2
