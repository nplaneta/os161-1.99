#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>

/*
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/*
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
/* static struct semaphore *intersectionSem; */
static struct lock *inIntersectionMutex;
static struct cv *openNorthEntry;
static struct cv *openSouthEntry;
static struct cv *openEastEntry;
static struct cv *openWestEntry;

const int numberOfCarsUntilSwitch = 8;

static int curDir = -1;

typedef struct directionStats
{
  int volatile carsWaiting;
  int volatile carsInIntersection;
  int volatile carsPastSinceLastWait;
  int associatednumber;
  struct cv *associatedCV;
} directionStats;

directionStats northDir;
directionStats southDir;
directionStats eastDir;
directionStats westDir;

directionStats maxDirection(directionStats num1, directionStats num2);

directionStats maxLastWait(directionStats num1, directionStats num2);



/*
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 *
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */

  /* intersectionSem = sem_create("intersectionSem",1); */
  inIntersectionMutex = lock_create("inIntersectionMutex");
  if (inIntersectionMutex == NULL) panic("could not create inIntersectionMutex lock");
  openNorthEntry = cv_create("openNorthEntry");
  if (openNorthEntry == NULL) panic("could not create N cv");
  openSouthEntry = cv_create("openSouthEntry");
  if (openSouthEntry == NULL) panic("could not create S cv");
  openEastEntry = cv_create("openEastEntry");
  if (openEastEntry == NULL) panic("could not create E cv");
  openWestEntry = cv_create("openWestEntry");
  if (openWestEntry == NULL) panic("could not create W cv");

  northDir.carsWaiting = 0;
  northDir.carsInIntersection = 0;
  northDir.associatednumber = 0;
  northDir.carsPastSinceLastWait = 0;
  northDir.associatedCV = openNorthEntry;

  southDir.carsWaiting = 0;
  southDir.carsInIntersection = 0;
  southDir.associatednumber = 1;
  southDir.carsPastSinceLastWait = 0;
  southDir.associatedCV = openSouthEntry;

  eastDir.carsWaiting = 0;
  eastDir.carsInIntersection = 0;
  eastDir.associatednumber = 2;
  eastDir.carsPastSinceLastWait = 0;
  eastDir.associatedCV = openEastEntry;

  westDir.carsWaiting = 0;
  westDir.carsInIntersection = 0;
  westDir.associatednumber = 3;
  westDir.carsPastSinceLastWait = 0;
  westDir.associatedCV = openWestEntry;

  return;
}

/*
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  /* KASSERT(intersectionSem != NULL); */
  KASSERT(inIntersectionMutex != NULL);
  lock_destroy(inIntersectionMutex);
  KASSERT(openNorthEntry != NULL);
  cv_destroy(openNorthEntry);
  KASSERT(openSouthEntry != NULL);
  cv_destroy(openSouthEntry);
  KASSERT(openEastEntry != NULL);
  cv_destroy(openEastEntry);
  KASSERT(openWestEntry != NULL);
  cv_destroy(openWestEntry);
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination)
{
  /* replace this default implementation with your own implementation */
  /* (void)origin;  /1* avoid compiler complaint about unused parameter *1/ */
  /* (void)destination; /1* avoid compiler complaint about unused parameter *1/ */
  /* KASSERT(intersectionSem != NULL); */
  /* P(intersectionSem); */
  (void)destination;
  /* KASSERT(inIntersectionMutex != NULL); */
  lock_acquire(inIntersectionMutex);
    if (origin == north) {
        if((curDir != 0 && curDir != -1) || southDir.carsInIntersection != 0 || eastDir.carsInIntersection != 0 || westDir.carsInIntersection != 0) {
          northDir.carsWaiting++;
          cv_wait(openNorthEntry, inIntersectionMutex);
        }
        if (curDir == -1) curDir = 0;
        northDir.carsPastSinceLastWait = 0;

        if(southDir.carsWaiting != 0) {
          southDir.carsPastSinceLastWait++;
        }
        if(eastDir.carsWaiting != 0) {
          eastDir.carsPastSinceLastWait++;
        }
        if(westDir.carsWaiting != 0) {
          westDir.carsPastSinceLastWait++;
        }
        northDir.carsWaiting = 0;
        northDir.carsInIntersection++;
    }
    else if (origin == south) {
        if((curDir != 1 && curDir != -1) || northDir.carsInIntersection != 0 || eastDir.carsInIntersection != 0 || westDir.carsInIntersection != 0) {
          southDir.carsWaiting++;
          cv_wait(openSouthEntry, inIntersectionMutex);
        }
        if (curDir == -1) curDir = 1;
        southDir.carsPastSinceLastWait = 0;
        if(northDir.carsWaiting != 0) {
          northDir.carsPastSinceLastWait++;
        }
        if(eastDir.carsWaiting != 0) {
          eastDir.carsPastSinceLastWait++;
        }
        if(westDir.carsWaiting != 0) {
          westDir.carsPastSinceLastWait++;
        }
        southDir.carsWaiting = 0;
        southDir.carsInIntersection++;
    }
    else if (origin == east) {
        if((curDir != 2 && curDir != -1) || northDir.carsInIntersection != 0 || westDir.carsInIntersection != 0 || southDir.carsInIntersection != 0) {
          eastDir.carsWaiting++;
          cv_wait(openEastEntry, inIntersectionMutex);
        }
        if (curDir == -1) curDir = 2;
        eastDir.carsPastSinceLastWait = 0;
        if(northDir.carsWaiting != 0) {
          northDir.carsPastSinceLastWait++;
        }
        if(southDir.carsWaiting != 0) {
          southDir.carsPastSinceLastWait++;
        }
        if(westDir.carsWaiting != 0) {
          westDir.carsPastSinceLastWait++;
        }
        eastDir.carsWaiting = 0;
        eastDir.carsInIntersection++;
    }
    else if (origin == west) {
        if((curDir != 3 && curDir != -1) || northDir.carsInIntersection != 0 || southDir.carsInIntersection != 0 || westDir.carsInIntersection != 0) {
          westDir.carsWaiting++;
          cv_wait(openWestEntry, inIntersectionMutex);
        }
        if (curDir == -1) curDir = 3;
        westDir.carsPastSinceLastWait = 0;
        if(northDir.carsWaiting != 0) {
          northDir.carsPastSinceLastWait++;
        }
        if(southDir.carsWaiting != 0) {
          southDir.carsPastSinceLastWait++;
        }
        if(eastDir.carsWaiting != 0) {
          eastDir.carsPastSinceLastWait++;
        }
        westDir.carsWaiting = 0;
        westDir.carsInIntersection++;
    }
  lock_release(inIntersectionMutex);
}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

directionStats maxDirection(directionStats num1, directionStats num2)
{
  if(num1.carsWaiting > num2.carsWaiting) {
    return num1;
  } else {
    return num2;
  }
}

directionStats maxLastWait(directionStats num1, directionStats num2)
{
  if(num1.carsPastSinceLastWait > num2.carsPastSinceLastWait) {
    return num1;
  } else {
    return num2;
  }
}


void
intersection_after_exit(Direction origin, Direction destination)
{
  /* replace this default implementation with your own implementation */
  /* (void)origin;  /1* avoid compiler complaint about unused parameter *1/ */
  /* (void)destination; /1* avoid compiler complaint about unused parameter *1/ */
  /* KASSERT(intersectionSem != NULL); */
  /* V(intersectionSem); */
  /* KASSERT(inIntersectionMutex != NULL); */
  (void)destination;
  lock_acquire(inIntersectionMutex);
    if (origin == north) {
      northDir.carsInIntersection--;
    }
    else if (origin == south) {
      southDir.carsInIntersection--;
    }
    else if (origin == east) {
      eastDir.carsInIntersection--;
    }
    else {
      westDir.carsInIntersection--;
    }
    /* if (northDir.carsWaiting == 0 && southDir.carsWaiting == 0 && eastDir.carsWaiting == 0 && westDir.carsWaiting == 0) {} */
    if (northDir.carsInIntersection == 0 && southDir.carsInIntersection == 0 && eastDir.carsInIntersection == 0 && westDir.carsInIntersection == 0) {
      if (northDir.carsWaiting == 0 && southDir.carsWaiting == 0 && eastDir.carsWaiting == 0 && westDir.carsWaiting == 0) {
        curDir = -1;
        lock_release(inIntersectionMutex);
        return;
      }

      //initial idea was to just switch to the thread with with most waiting cars
      //but, some threads were starving. So, force a switch after a thread hasn't been picked 8+ times

      // might switch to a thread with no waitingcars, and it gets stuck...? (DONT KNOW WHY ASK)
      // Made sure in before intersection that if carsPastSinceLastWait > 0, there are actually cars waiting to deal with no waitingcars
      // which is to say carsWaiting > 0 for direction

      // So below pick the one that's been waiting the longest and open it up
      directionStats newDirectionWait = maxLastWait(maxLastWait(northDir, southDir), maxLastWait(eastDir, westDir));
      if(newDirectionWait.carsPastSinceLastWait >= numberOfCarsUntilSwitch) {
        curDir = newDirectionWait.associatednumber;
        cv_broadcast(newDirectionWait.associatedCV, inIntersectionMutex);
      } else {
        // if none haven''t been picked 20+ times, pick thread with most waiting
        directionStats newDirection = maxDirection(maxDirection(northDir, southDir), maxDirection(eastDir, westDir));
        curDir = newDirection.associatednumber;
        cv_broadcast(newDirection.associatedCV, inIntersectionMutex);
      }
    }
  lock_release(inIntersectionMutex);
}
