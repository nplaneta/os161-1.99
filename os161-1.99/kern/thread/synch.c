/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Synchronization primitives.
 * The specifications of the functions are in synch.h.
 */

#include <types.h>
#include <lib.h>
#include <spinlock.h>
#include <wchan.h>
#include <thread.h>
#include <current.h>
#include <synch.h>

// // remove later, just so vscode doesn't whine at me. Ask prof about why it's complaining.
// #include <stdbool.h>

////////////////////////////////////////////////////////////
//
// Semaphore.

struct semaphore *
sem_create(const char *name, int initial_count)
{
        struct semaphore *sem;

        KASSERT(initial_count >= 0);

        sem = kmalloc(sizeof(struct semaphore));
        if (sem == NULL) {
                return NULL;
        }

        sem->sem_name = kstrdup(name);
        if (sem->sem_name == NULL) {
                kfree(sem);
                return NULL;
        }

	sem->sem_wchan = wchan_create(sem->sem_name);
	if (sem->sem_wchan == NULL) {
		kfree(sem->sem_name);
		kfree(sem);
		return NULL;
	}

	spinlock_init(&sem->sem_lock);
        sem->sem_count = initial_count;

        return sem;
}

void
sem_destroy(struct semaphore *sem)
{
        KASSERT(sem != NULL);

	/* wchan_cleanup will assert if anyone's waiting on it */
	spinlock_cleanup(&sem->sem_lock);
	wchan_destroy(sem->sem_wchan);
        kfree(sem->sem_name);
        kfree(sem);
}

void 
P(struct semaphore *sem)
{
        KASSERT(sem != NULL);

        /*
         * May not block in an interrupt handler.
         *
         * For robustness, always check, even if we can actually
         * complete the P without blocking.
         */
        KASSERT(curthread->t_in_interrupt == false);

	spinlock_acquire(&sem->sem_lock);
        while (sem->sem_count == 0) {
		/*
		 * Bridge to the wchan lock, so if someone else comes
		 * along in V right this instant the wakeup can't go
		 * through on the wchan until we've finished going to
		 * sleep. Note that wchan_sleep unlocks the wchan.
		 *
		 * Note that we don't maintain strict FIFO ordering of
		 * threads going through the semaphore; that is, we
		 * might "get" it on the first try even if other
		 * threads are waiting. Apparently according to some
		 * textbooks semaphores must for some reason have
		 * strict ordering. Too bad. :-)
		 *
		 * Exercise: how would you implement strict FIFO
		 * ordering?
		 */
		wchan_lock(sem->sem_wchan);
		spinlock_release(&sem->sem_lock);
                wchan_sleep(sem->sem_wchan);

		spinlock_acquire(&sem->sem_lock);
        }
        KASSERT(sem->sem_count > 0);
        sem->sem_count--;
	spinlock_release(&sem->sem_lock);
}

void
V(struct semaphore *sem)
{
        KASSERT(sem != NULL);

	spinlock_acquire(&sem->sem_lock);

        sem->sem_count++;
        KASSERT(sem->sem_count > 0);
	wchan_wakeone(sem->sem_wchan);

	spinlock_release(&sem->sem_lock);
}

////////////////////////////////////////////////////////////
//
// Lock.

struct lock *
lock_create(const char *name)
{
        struct lock *lock;

        lock = kmalloc(sizeof(struct lock));
        if (lock == NULL) {
                return NULL;
        }

        lock->lk_name = kstrdup(name);
        if (lock->lk_name == NULL) {
                kfree(lock);
                return NULL;
        }
        
        // add stuff here as needed
	//
	lock->lk_wchan = wchan_create(lock->lk_name);
        if (lock->lk_wchan == NULL) {
                kfree(lock->lk_name);
                kfree(lock);
                return NULL;
        }

	spinlock_init(&lock->lk_lock);
	lock->lk_count = 0;
	lock->lk_thread = NULL;
        
        return lock;
}

void
lock_destroy(struct lock *lock)
{
        KASSERT(lock != NULL);

        // add stuff here as needed
	spinlock_cleanup(&lock->lk_lock);
	wchan_destroy(lock->lk_wchan);
        
        kfree(lock->lk_name);
        kfree(lock);
}

void
lock_acquire(struct lock *lock)
{
	KASSERT(lock != NULL);
        KASSERT(!lock_do_i_hold(lock));
        KASSERT(curthread->t_in_interrupt == false); //from semaphore
        spinlock_acquire(&lock->lk_lock); //from semaphore
        while (lock->lk_count == 1) { //must be in while in case someone grabs the spinlock between time that you woke up and tried to acquire lock
           wchan_lock(lock->lk_wchan); // need to modify wchan so lock it before releasing spinlock. We lock it so sleep can unlock it
           spinlock_release(&lock->lk_lock);
           wchan_sleep(lock->lk_wchan); // will release wchan spinlock for thread then sleeps, when thread is woken it returns

           spinlock_acquire(&lock->lk_lock); //reacquire our own personal spinlock on this lock
        }
        KASSERT(lock->lk_count == 0); // if nobody has it (we have lock so this is unchanged
        lock->lk_count = 1; //grab lock
        lock->lk_thread = curthread; //owner is current thread
        spinlock_release(&lock->lk_lock); //release spinlock, others can now modify.
}

void
lock_release(struct lock *lock)
{
        // Write this
	//
	// so while we have spinlock no changes to this lock can be made. And we only ever do this for short periods of time
	// so inefficiency of spinlock is ok

        KASSERT(lock != NULL);

	spinlock_acquire(&lock->lk_lock);

	KASSERT(lock->lk_count == 1);
	KASSERT(lock->lk_thread == curthread);

        lock->lk_count = 0;
	lock->lk_thread = NULL;        // what happens if I make this NULL after waking a thread?
        KASSERT(lock->lk_count == 0); //redundant cause spinlock but eh
	wchan_wakeone(lock->lk_wchan);

	spinlock_release(&lock->lk_lock);
}

bool
lock_do_i_hold(struct lock *lock)
{
        // Write this
	//
	KASSERT(lock != NULL);

        spinlock_acquire(&lock->lk_lock);

	if(lock->lk_count == 1 && lock->lk_thread == curthread) {
		spinlock_release(&lock->lk_lock);
		return true;
	} else {
		spinlock_release(&lock->lk_lock);
		return false;
	}

}

////////////////////////////////////////////////////////////
//
// CV


struct cv *
cv_create(const char *name)
{
        struct cv *cv;

        cv = kmalloc(sizeof(struct cv));
        if (cv == NULL) {
                return NULL;
        }

        cv->cv_name = kstrdup(name);
        if (cv->cv_name==NULL) {
                kfree(cv);
                return NULL;
        }
        
        // add stuff here as needed

        //wchan create took in name with semaphores, but I don't actually know why...? Was unable to find in slides
        cv->cv_wchan = wchan_create(cv->cv_name);
	if (cv->cv_wchan == NULL) {
		kfree(cv->cv_name);
		kfree(cv);
		return NULL;
	}
        
        return cv;
}

void
cv_destroy(struct cv *cv)
{
        KASSERT(cv != NULL);

        // add stuff here as needed

        wchan_destroy(cv->cv_wchan);
        
        kfree(cv->cv_name);
        kfree(cv);
}

void
cv_wait(struct cv *cv, struct lock *lock)
{
        KASSERT(cv != NULL);
        KASSERT((curthread->t_in_interrupt  == false));
        
        // while(!lock_do_i_hold(lock))    WHY DID THIS WHILE LOOP NOT WORK???
        wchan_lock(cv->cv_wchan); // lock wchan so nobody wakes up while we're releasing the lock and grabs it i think?
        lock_release(lock);
	wchan_sleep(cv->cv_wchan); // released wchan lock. Do I grab it right away when I wake up so nobody could steal lock?
        lock_acquire(lock); // regrabs lock. Could someone else grab lock before we wake...?

        //signalling is separate

}

void
cv_signal(struct cv *cv, struct lock *lock)
{
        KASSERT(cv != NULL);
	KASSERT(lock_do_i_hold(lock));
        KASSERT(lock != NULL);

        wchan_wakeone(cv->cv_wchan);

}

void
cv_broadcast(struct cv *cv, struct lock *lock)
{
        KASSERT(cv != NULL);
        KASSERT(lock_do_i_hold(lock));
        KASSERT(lock != NULL);
	// Write this
	wchan_wakeall(cv->cv_wchan);
}
